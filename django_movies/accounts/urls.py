from django.urls import path

from .views import SubmittableLoginView, SubmittablePasswordChangeView, SuccessMessagedLogoutView, SignUpView

app_name = 'accounts'
urlpatterns = [
    path('login/', SubmittableLoginView.as_view(), name='login'),
    path('passchange/', SubmittablePasswordChangeView.as_view(), name='password_change'),
    path('logout/', SuccessMessagedLogoutView.as_view(), name='logout'),
    path('signup/', SignUpView.as_view(), name='signup')
]
