import pytest

from core.models import Director, Genre, Movie
from rest_framework.test import APIClient


@pytest.fixture
def director():
    return Director.objects.create(name='Mateusz', surname='Gorski')


@pytest.fixture
def genre():
    return Genre.objects.create(name='Action')


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def movies(director, genre):
    return Movie.objects.bulk_create([
        Movie(
            director=director,
            genre=genre,
            title='Title 1',
            released='2020-08-20'
        ),
        Movie(
            director=director,
            genre=genre,
            title='Title 2',
            released='2020-08-20'
        ),
        Movie(
            director=director,
            genre=genre,
            title='Title 3',
            released='2020-08-20'
        )
    ])
