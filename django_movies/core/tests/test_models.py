import pytest

from core.models import Genre, Movie, Director
from django.db import IntegrityError
from django.urls import reverse


@pytest.mark.django_db
def test_genre_creation():
    initial_count = Genre.objects.count()
    Genre.objects.create(name='action')
    count = Genre.objects.count()
    assert initial_count + 1 == count
    assert Genre.objects.first().name == 'action'


@pytest.mark.django_db
def test_movie(director, genre):
    assert  Movie.objects.all().count() == 0
    movie = Movie(
        director=director,
        genre=genre,
        title='Forest Gump',
        released='2020-02-02'
    )
    movie.save()
    assert Movie.objects.all().count() == 1


@pytest.mark.django_db
def test_director():
    with pytest.raises(IntegrityError):
        Director.objects.create(name='John')


@pytest.mark.django_db
def test_movie_list(api_client, movies):
    url = reverse('core:movie_list')
    response = api_client.get(url, follow=True, format='json')
    assert response.status_code == 200
    data = response.context_data
    objects = data['object_list']
    object_titles = [obj.title for obj in objects]
    for movie in movies:
        assert movie.title in object_titles
